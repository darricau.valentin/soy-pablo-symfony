<?php

namespace App\Controller;

use App\Model\Song;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'songs' => [
                new Song('/audio/comamierda.mp3', 'Coma Mierda'),
                new Song('/audio/marica.mp3', 'Marica'),
                new Song('/audio/tomar.mp3', 'Tomar'),
                new Song('/audio/queputa.mp3', 'Que Puta'),
                new Song('/audio/cojones.mp3', 'Cojones'),
                new Song('/audio/cali.mp3', 'Cali'),
                new Song('/audio/castanos.mp3', 'Castaños'),
                new Song('/audio/sipatronsolo.mp3', 'Si Patrón'),
                new Song('/audio/muerte.mp3', 'Muerte'),
                new Song('/audio/elcartel.mp3', 'El Cartel'),
                new Song('/audio/hijitos.mp3', 'Hijitos'),
                new Song('/audio/hijodeputa.mp3', 'Hijo de Puta'),
                new Song('/audio/judy.mp3', 'Judy'),
                new Song('/audio/mensaje.mp3', 'Mensaje'),
                new Song('/audio/plata.mp3', 'Plata'),
                new Song('/audio/posey.mp3', 'Posey'),
                new Song('/audio/entrar.mp3', 'Entrar'),
                new Song('/audio/laugh.mp3', 'Risa'),
            ]
        ]);
    }
}
