# Soy Pablo 2.0

Around 2015, when the TV show Narcos was booming, we decided with a few friends to create a soundbox app with Ionic "Soy Pablo" (see the code [here](https://gitlab.com/darricau.valentin/pablo-sound-box))
containing a few of Pablo iconic sentences.

The app has been out of the playstore for a while, so I decided to revive it with a super simple symfony app, and deploy it here:
~~https://soypablo.herokuapp.com/~~. WEEEELL, plot twist, Heroku is not free anymore. So I took it down, build it in ReactJS (link to the repo [here](https://gitlab.com/darricau.valentin/soy-pablo-react)). You can find the app here https://soypablo.vercel.app/.

All rights for the sounds belong to Netflix, no profit is made from the website.

Have fun!
