import { Controller } from 'stimulus';

export default class extends Controller {
    static targets = ['songAudio'];

    connect() {
        this.songAudioTarget.load();
    }

    play() {
        console.log(this.songAudioTarget);
        this.songAudioTarget.play();
    }
}
