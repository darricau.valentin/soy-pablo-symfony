import { Controller } from 'stimulus';

export default class extends Controller {
    static targets = ['songAudio'];

    stop() {
        this.songAudioTargets.forEach((element) => {
            element.pause();
        })
    }
}
