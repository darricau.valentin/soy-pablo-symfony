import { Controller } from 'stimulus';

export default class extends Controller {
    static targets = ['currentImage'];

    index = 0;
    images = [
       'images/pablo_face.svg',
       'images/pablo_face1.svg',
       'images/pablo_face2.svg',
    ];

    connect() {
        this.index = Math.floor(Math.random() * 3);
        this.setImage();
    }

    changeImage(event) {
        event.preventDefault();

        this.index++;
        if (this.index > 2) {
            this.index = 0;
        }

        this.setImage();
    }

    setImage() {
        this.currentImageTarget.src = this.images[this.index];
    }
}
